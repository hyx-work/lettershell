#include "stm32f4xx.h"
#include "shell_cmd.h"

static SHELL_TypeDef usart1_shell;

static signed char com1_read(char * ch)
{
	while(!(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET))
	{
	}

	*ch = USART_ReceiveData(USART1);
	return 0;
}

static void com1_write(const char ch)
{
	USART_SendData(USART1, ch);
	/* Loop until the end of transmission */
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
	{}
}

void usart1_shell_init(void)
{
#if (SHELL_USING_TASK)
	usart1_shell.read = NULL;
#else
	usart1_shell.read = com1_read;
#endif
	usart1_shell.write = com1_write;
	shellInit(&usart1_shell);	

}

void* usart1_shell_instance(void)
{
	return (void*)&usart1_shell;	
}


/*******************************************************************************
*@function  shellRebot
*@brief     ????
*@param     None
*@retval    None
*@author    Letter
*******************************************************************************/

#if SHELL_AUTO_PRASE == 0
	void shellReboot(int argc, char *argv[])
	{
	    shellClear();
	    shellDisplay(&usart1_shell,"system rebooting 0\r\n");
	    __set_FAULTMASK(1);
	    NVIC_SystemReset();
	}

	void testFunc1(int argc, char *argv[])
	{
	    printf("%d parameter(s)\r\n", argc);
	    for (char i = 1; i < argc; i++)
	    {
	        printf("%s\r\n", argv[i]);
	    }
	}

	SHELL_EXPORT_CMD_EX(reboot, shellReboot, reboot system,reboot [command] --show reboot info of command);
	SHELL_EXPORT_CMD_EX(test1, testFunc1, test1 function as main sample,test [command] --show test1 info of command);
#else
	void shellReboot(void)
	{
	    shellClear();
	    shellDisplay(&usart1_shell,"system rebooting\r\n");
	    __set_FAULTMASK(1);
	    NVIC_SystemReset();
	}
 
	int multiParams(int i, char ch, char *str)
	{
	    printf("input int: %d, char: %c, string: %s\r\n", i, ch, str);
		return -2;
	}
	SHELL_EXPORT_CMD(reboot, shellReboot, reboot system);
	SHELL_EXPORT_CMD(multi, multiParams, int multiParams(int i, char ch, char *str));
#endif
	

