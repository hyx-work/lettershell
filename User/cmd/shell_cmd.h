#ifndef __SHELL_CMD_H__
#define __SHELL_CMD_H__
#include <stdio.h>
#include <stdlib.h>
#include "shell.h"

void usart1_shell_init(void);
void* usart1_shell_instance(void);

#endif
