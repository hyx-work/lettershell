 
#include "bsp.h"
#include "./cmd/shell_cmd.h"
#include "bsp_led.h"
#include "bsp_delay.h"



int main(void)
{

	BSP_Init();
	printf("Booting\r\n");
	printf("+------------------------------------------+\r\n");
	printf("|               APP Starting               |\r\n");
	printf("|      BUILD ON %s %s       |\r\n", __DATE__,__TIME__);
	printf("+------------------------------------------+\r\n");

	usart1_shell_init();

	while(1)
	{
		LED1_ON();
		delay_ms(500);
		LED1_OFF();
		delay_ms(500);
		LED0_Toggle();
	}	
}



/*********************************************END OF FILE**********************/

