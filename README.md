## 程序简介
 工程名称：LetterShell

 实验平台: 正点原子 阿波罗 STM32 F429 开发板 

 ST STD 固件库版本：1.8.0

## 功能简介：
 使用USART1收发数据并集成letter shell 2.x

 学习目的：集成一款shell，供开发使用

## 移植成果

![images](./lettershell.png)

**打赏**

![image](https://gitee.com/hyx-work/modbus-mqtt-bridge/raw/master/img/pay.png)

**本人微信**

![image](https://gitee.com/hyx-work/modbus-mqtt-bridge/raw/master/img/wx.jpg)