#include "bsp_sys.h"
#include "bsp_delay.h"
#include "bsp_dbg_usart.h"
#include "bsp_led.h"

void BSP_Init(void)
{
	// 初始化延时函数
	delay_init(180);               	 	
	// 嵌套向量中断控制器组选择
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	// 初始化USART 配置模式为 115200 8-N-1，中断接收
	DBG_Init();

	// LED 配置
	LED_GPIO_Config();
}
