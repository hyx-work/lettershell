#include "bsp_led.h"   
#include "bsp_sys.h"   
//引脚定义
/*******************************************************/
//绿色灯 DS1
#define LED0_PIN                  GPIO_Pin_0                 
#define LED0_GPIO_PORT            GPIOB                      
#define LED0_GPIO_CLK             RCC_AHB1Periph_GPIOB

//红色灯 DS0
#define LED1_PIN                  GPIO_Pin_1                 
#define LED1_GPIO_PORT            GPIOB                      
#define LED1_GPIO_CLK             RCC_AHB1Periph_GPIOB

/************************************************************/

 /**
  * @brief  初始化控制LED的IO
  * @param  无
  * @retval 无
  */
void LED_GPIO_Config(void)
{		
	/*定义一个GPIO_InitTypeDef类型的结构体*/
	GPIO_InitTypeDef GPIO_InitStructure;

	/*开启LED相关的GPIO外设时钟*/
	RCC_AHB1PeriphClockCmd (LED0_GPIO_CLK, ENABLE); 

	/*选择要控制的GPIO引脚*/
	GPIO_InitStructure.GPIO_Pin = LED0_PIN;	

	/*设置引脚模式为输出模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;   

	/*设置引脚的输出类型为推挽输出*/
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

	/*设置引脚为上拉模式*/
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	/*设置引脚速率为2MHz */   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; 

	/*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
	GPIO_Init(LED0_GPIO_PORT, &GPIO_InitStructure);	
	LED0_OFF();


	/*选择要控制的GPIO引脚*/
	GPIO_InitStructure.GPIO_Pin = LED1_PIN;	

	/*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
	GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);	
	LED1_OFF();
	

}

/* 直接操作寄存器的方法控制IO */
#define	digitalHi(p,i)			 {p->BSRRL=i;}		//设置为高电平
#define digitalLo(p,i)			 {p->BSRRH=i;}		//输出低电平
#define digitalToggle(p,i)	 {p->ODR ^=i;}		//输出反转状态

void LED0_Toggle(void)
{
	digitalToggle(LED0_GPIO_PORT,LED0_PIN);
}

void LED0_ON(void)
{
	digitalHi(LED0_GPIO_PORT,LED0_PIN)
}

void LED0_OFF(void)
{
	digitalLo(LED0_GPIO_PORT,LED0_PIN)
}

void LED1_Toggle(void)
{
	digitalToggle(LED1_GPIO_PORT,LED1_PIN);
}

void LED1_ON(void)
{
	GPIO_ResetBits(LED1_GPIO_PORT,LED1_PIN);
}

void LED1_OFF(void)
{
	GPIO_SetBits(LED1_GPIO_PORT,LED1_PIN);
}


