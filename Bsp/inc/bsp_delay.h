#ifndef __BSP_DELAY_H__
#define __BSP_DELAY_H__
#include "stm32f4xx.h"

#define SYSTEM_SUPPORT_OS 0
	 
void delay_init(u8 SYSCLK);
void delay_ms(u16 nms);
void delay_us(u32 nus);

#endif

