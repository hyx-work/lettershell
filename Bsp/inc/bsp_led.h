#ifndef __BSP_LED_H__
#define	__BSP_LED_H__

#include "stm32f4xx.h"

void LED_GPIO_Config(void);

void LED0_ON(void);
void LED0_OFF(void);
void LED0_Toggle(void);

void LED1_ON(void);
void LED1_OFF(void);
void LED1_Toggle(void);



#endif

