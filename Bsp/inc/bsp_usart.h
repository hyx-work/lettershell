#ifndef __BSP_USART_H__
#define	__BSP_USART_H__

#include "stm32f4xx.h"

/*****************  发送一个字符 **********************/
void USART_SendByte( USART_TypeDef * pUSARTx, uint8_t ch);


/*****************  发送字符串 **********************/
void USART_SendString( USART_TypeDef * pUSARTx, char *str);


/*****************  发送一个16位数 **********************/
void USART_SendHalfWord( USART_TypeDef * pUSARTx, uint16_t ch);

#endif

